# Cavans javasript Library #

A javascript library filled with functions which simplify development proccesses.

## What is this repository for? ##

* Extending javascript / jquery for commonly used functions.

## How do I get set up? ##

* Clone the project into a folder
* npm-install to install dependencies
* Use gulp for the taskrunner. 
* Check the gulpfile for usable tasks


## Inflate / deflate ##

The purpose of inflate is to allow jquery to scale a element. it accepts 2 parameters.

* Number from 1 - 10.
* true or false depending on whether or not you want the element to inflate or deflate.

#### Example ####
This example deflates the element and does it within a timeframe of 2 seconds
```
$('.testElem').inflate(2,false);
```

## animateScroll ##

Animate scroll sets the parent's element to 'relative' and does an animate scroll to the selected element. Included in the code is an extension to the jquery library that extends easing.

The extensions are as follows:
```
easeInQuad, easeOutQuad, easeInOutQuad, easeInCubic, easeOutCubic:, easeInOutCubic, easeInQuart, easeOutQuart, easeInOutQuart, easeInQuint, easeOutQuint, easeInOutQuint, easeInSine, easeOutSine, easeInOutSine, easeInExpo, easeOutExpo, easeInOutExpo, easeInCirc, easeOutCirc, easeInOutCirc, easeInElastic, easeOutElastic, easeInOutElastic, easeInBack, easeOutBack, easeInOutBack, easeInBounce, easeOutBounce, easeInOutBounce
```
#### This can be used for ####
* A waypoint inside of a element with overflow scroll
* To scroll to a particular point inside of a page

#### Example ####

```
$('.scrollHere').animateScroll(500,'easeInBounce');
```

## Validation Check ##

This is a check for both email and text fields. It will return a green colour for true and red colour for false

#### Example ####

This example demonstrates that you can include your own validation colours in the function and run a callback oncomplete of running this code.
You can include a optional parameter of nocolour where it will revert the border back to its original colour.
All of the options inside the second parameter are **optional**

```
$('.fieldWrapper').isFormValid(function(){
	alert("fields are valid!");
},{failColour: '#000', successColour: '#FFF', noColour: true});
```

## Slidein ##

This function is for the use of animating a element to go to a certain position on the page. This uses greensocks and is able to take in different easing options from the options panel

#### Example ####

This parameters of this code is as follows: 

* Time of animation
* options for positioning element

Options include:

* Top
* left
* bottom
* right
* Easing

```
$('#slideWrapper .elem').slideIn(2,{left: '0', top: "150px", easing: Power3.easeInOut});
```



### Who do I talk to about issues? ###

* Let the cavman know of any issues ;P