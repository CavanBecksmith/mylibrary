var TEST = {
	init: function(){
		this.galleryStart();
		this.setupPositions();
		this.addEventListeners();
		this.parralaxStart();
		this.drawSvg();
		this.buildCta();
		this.buildSlides();
		this.buildBut();
		this.buildNavigation();
	},
	addEventListeners: function(){
		var self=this;
		// Inflate and deflate
		$('.inflate_in').click(function(){
			$('.testElem').inflate(2,true);
		});
		$('.inflate_out').click(function(){
			$('.testElem').inflate(2,false);
		});		

		// Animate scroll
		$('.gotToTop').click(function(){
			$('.top').animateScroll(500,'swing');
		});
		$('.gotToBot').click(function(){
			$('.scrollHere').animateScroll(500,'easeInOutBounce');
		});

		// Check validation 
		$('.validClick').click(function(){
			$('.fieldWrapper').isFormValid(function(){
				alert("fields are valid!");
			},{failColour: 'red', successColour: 'green', noColour: false});
		});
		
		$('.slideMeIn').click(function(){
			$('#slideWrapper .elem').slideIn(2,{left: '0', top: "150px", easing: Power3.easeInOut});
		});

		// Draw SVG
		$('.submitDraw').click(function(){
			// $('.drawSvg').drawSvg();
			self.logo
			  .stop()
			  .reset()
			  .play();

			self.svgTwo 
				.stop()
				.reset()
				.play();
		});

	},

	setupPositions: function(){
		// SlideWrapper
		$('#slideWrapper').css({'position':'relative'}).find('.elem').css({'position':'absolute', 'left':'300px'});
	},
	dateInput: function(){
	},

	galleryStart: function(){
		$('.gallery').slideShow(["img/img1.jpg", "img/img2.jpg", "img/img3.jpg", "img/img4.jpg", "img/img5.jpg", "img/img6.jpg"], {width:"100%"});
	},
	parralaxStart: function(){
		$('.parallax-window').parallax({imageSrc: 'img/img2.jpg'});

	},
	buildCta: function(){
		$('.ctaBuild').ctaBuild({width:'500px', text: 'Some text goes here.'});
	},
	buildSlides: function(){
		var options = {
			html: [
			'<h1> Some text</h1><p>This is some html</p>',
			'<h1> Some text</h1><p>This is some html</p>',
			'<h1> Some text</h1><p>This is some html</p>',
			'<h1> Some text</h1><p>This is some html</p>'
			],
			shuffle: true
		}
		$('#slide').buildSlides(2,2,'panelWrapper',options);
	},
	buildNavigation: function(){
		$('#navigationBuild').navigationBuild([["Home","#.html"],["About","#.html"]]);
	},
	logo: null,
	svgTwo: null,
	drawSvg: function(){
		// $('.drawSvg').drawSvg();
		// new Vivus('Layer_1', {type: 'async', duration: 200}, function(){
		//   // document.getElementById("container").className = "white";
		//   console.log('running');
		// });
		// new Vivus('svgTwo', {type: 'async', duration: 200}, function(){
		//   // document.getElementById("container").className = "white";
		//   console.log('running2 ');
		// });		

		this.logo = new Vivus('logo', {type: 'async', duration: 200}, function(){
		  // document.getElementById("container").className = "white";
		});

		this.svgTwo = new Vivus('svgTwo', {type: 'async', duration: 200}, function(){});

	},
	buildBut: function(){
		$('#buildBtn').buildBtn("Text", {css: {background: '#eee'}}, function(){alert('Click')});
	}

}

$(document).ready(function(){
	TEST.init();
});