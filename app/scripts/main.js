(function( $ ) {
    
    // Scales element to 0 or 1;
    $.fn.inflate = function(time,action) {
        var self=this;
        var defTime = 5;
        var output = {
            init: function(){
                // console.log('init');
                for(var i = 0; i <= 10; i++){
                    if(self.hasClass('inflate_trans_'+i)){
                        self.removeClass('inflate_trans_'+i);
                    }
                }
                this.updateTime();
                this.doAnimation()
            },
            doAnimation: function(){
                // console.log('doAnimation');
                if(action == true){
                    this.inflate();
                }
                else{
                    this.deflate();
                }
            },
            inflate: function(){
                // Minimizes the element
                // Remove previous transition class
                self.removeClass('deflate');
                self.addClass('inflate');
            },
            deflate: function(){
                // Maximizes the element
                self.removeClass('inflate');
                self.addClass('deflate');
            },
            updateTime: function(){
                if(typeof(time) == 'number' && time < 100){
                    defTime = time;
                }
                self.addClass('inflate_trans_'+defTime);
            }
        };

        output.init();
 
    };

    // Animates scroll to a top position of the selected element
    $.fn.animateScroll = function(duration,easing){
        var self=this;
        var defTime = 500;
        var parent=null;
        var output = {
            init: function(){
                parent=self.parent();
                this.calcTime();
                this.animate();
            },
            animate: function(){
                parent.css('position','relative').animate({scrollTop:(parent.scrollTop() + self.position().top)}, duration, easing);
            },
            calcTime: function(){

            }
        };

        output.init();
    }
    // Checks validation on required input boxes
    $.fn.isFormValid = function(callback,options){
        var self=this;
        var counter=0;

        var output = {
            init: function(){
                // Loops the inputs inside of the selected object and checks to see if their valid depending on type of input.
                self.find("input[required]").each(function(){
                    var valid=true;
                    var type=$(this).attr("type");
                    // If the input type is text
                    if(type=="text"){
                        if(valid && $.trim($(this).val())==""){
                            valid=false;
                            $(this).css({'border':'1px solid '+options.failColour});
                        }
                        else{
                            if(options.noColour == true){
                                $(this).css({'border':""});
                            }
                            else{                         
                                $(this).css({'border':"1px solid "+options.successColour});
                                counter++;
                            }
                        }                        
                    }
                    // If the input typ
                    else if(type=="email"){
                        if(valid && ($.trim($(this).val())=="" || $(this).val().indexOf("@")==-1 || $(this).val().indexOf(".")==-1)){
                            valid=false;
                            $(this).css({'border':'1px solid '+options.failColour});
                        }
                        else{
                            if(options.noColour == true){
                                $(this).css({'border':""});
                            }
                            else{                            
                                $(this).css({'border':"1px solid "+options.successColour});
                                counter++;
                            }
                        }
                    }
                    // If the input type is a date
                    else if(type=="date"){
                        var date = $('.date').val();
                        console.log('this is a date',$('.date').val());
                                             
                    }
                    // If the input type is a password
                    else if(type=="password"){
                        // console.log('This is a password',$('.password').val());
                        var regx=  /^[A-Za-z]\w{7,14}$/;  
                        var password=$('.password').val();
                        if(password.match(regx))   
                        {   
                            alert('Correct, try another...')  
                            return true;  
                        }  
                        else  
                        {   
                            alert('Wrong...!')  
                            return false;  
                        }
                    }
                });
                if(self.find('input[required]').length == counter){
                    this.runCallback();
                }
                else{
                    console.log("Fields are not valid");
                }
            },
            runCallback: function(){
                callback();
            },
            checkOptions: function(){
                if(typeof(options) == 'undefined'){
                    options = new Object();
                    options.failColour = '#D9534F';
                    options.successColour = 'green';
                    options.noColour = false;
                }
                else{
                    console.log(options);
                    options.failColour = options.failColour || '#D9534F';
                    options.successColour = options.successColour || 'green';
                    options.noColour = options.noColour || false;
                }
            }

        }
        output.checkOptions();
        output.init();
    }
    // Greensock functions

    // Animates a element within a timeframe using set options
    $.fn.slideIn = function(time,options){
        var self=this;
        var tl = new TimelineLite();
        console.log(tl);

        var output = {
            init: function(){
                this.options();
                this.animate();
            },
            animate: function(){
                TweenMax.to(self, 2, {top:options.top, right:options.right, bottom:options.bottom, left:options.left, ease:options.easing});
            },
            options: function(){
                if(typeof(options) == 'undefined'){
                    options = new Object();
                    options.top = self.position().top;
                    options.bottom = self.position().bottom;
                    options.left = self.position().left;
                    options.right = self.position().right;
                    options.easing = Power0.easeNone;
                }
                else{
                    options.top = options.top || 0;
                    options.bottom = options.bottom || 0;
                    options.left = options.left || 0;
                    options.right = options.right || 0;
                    options.easing = options.easing || Power0.easeNone;
                }                
            }
        }

        output.init();
    }
    // Using jcarousel - Jquery plugin
    $.fn.slideShow = function(arr,options){
        var self=this;
        var html = {
            start: '<div class="jcarousel-wrapper"><div class="jcarousel"><ul>',
            end: '</ul></div><a href="#" class="jcarousel-control-prev">&lsaquo;</a><a href="#" class="jcarousel-control-next">&rsaquo;</a><p class="jcarousel-pagination"></p></div>',
            completed: '',
            listItems: null
        }
        var output = {
            init: function(){
                this.options();
                // this.template();
                
                
                // this.buildTest();
            },
            template: function(){
            },
            options: function(){
                if(typeof(options) == 'undefined'){
                    options = new Object();
                    options.width = 600;
                    options.height = 400;
                    options.breakPoints = [[600,1], [350,1]];
                }
                else{
                    options.width = options.width || 600;
                    options.height = options.height || 400;
                    options.breakPoints = options.breakPoints || [[600,1], [350,1]];
                }  

                this.buildWrapper();
            },
            buildWrapper: function(){
                var imgHolder = '';
                html.listItems = arr;

                for(var i = 0; i < html.listItems.length; i++){
                    imgHolder += '<li><img src="'+html.listItems[i]+'" width="'+options.width+'" height="'+options.height+'" alt=""></li>';
                }

                html.completed = html.start + imgHolder + html.end;

                self.append(html.completed);
                this.build();
            },
            build: function(){
                // Build

                (function($) {
                    $(function() {
                        var jcarousel = $('.jcarousel');

                        jcarousel
                            .on('jcarousel:reload jcarousel:create', function () {
                                var carousel = $(this),
                                    width = carousel.innerWidth();

                                if (width >= options.breakPoints[0][0]) {
                                    width = width / options.breakPoints[0][1];
                                } else if (width >= options.breakPoints[1][0]){
                                    width = width / options.breakPoints[1][1];
                                }

                                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
                            })
                            .jcarousel({
                                wrap: 'circular'
                            });

                        $('.jcarousel-control-prev')
                            .jcarouselControl({
                                target: '-=1'
                            });

                        $('.jcarousel-control-next')
                            .jcarouselControl({
                                target: '+=1'
                            });

                        $('.jcarousel-pagination')
                            .on('jcarouselpagination:active', 'a', function() {
                                $(this).addClass('active');
                            })
                            .on('jcarouselpagination:inactive', 'a', function() {
                                $(this).removeClass('active');
                            })
                            .on('click', function(e) {
                                e.preventDefault();
                            })
                            .jcarouselPagination({
                                perPage: 1,
                                item: function(page) {
                                    return '<a href="#' + page + '">' + page + '</a>';
                                }
                            });
                    });
                })(jQuery);


                // Build
            }

        }

        output.init();
    },


    $.fn.ctaBuild = function(options){
        var self=this;
        var output = {
            init: function(){
                this.buildWrapper();
            },
            buildWrapper: function(){
                var html = ''
            },
            options:{    
                width: '200px',
                height: '100px',
                background: '#FFF',
                font_colour: '#000',
                border: '1px solid #000',
                class: 'ctaBtn', 
                text: 'Click Here'
            }

        }

        if(options!=='undefined'){
            $.extend(true,output.options,options);
        }

        output.init();
        // console.log(output.options);
    },


    $.fn.drawSvg = function(time,link,callback){
        var self=this
        var output = {
            init: function(){
                new Vivus(self.attr('id'), {duration: time, file: link}, callback());
            },
        }
    }


    $.fn.buildSlides = function(cols,rows,id,optionsInput){
        var wrapper=this;
        var slideWrapper = null;
        var percentage=null;
        var shuffleArr = {
            runEach: false,
            arr: null
        };
        var options = {
            css: {
                'background':'#eeeeee',
                'margin_left':'1%',
                'margin_right':'1%',
                'margin_top':'1%',
                'margin_bottom':'1%',
                'padding': '1%',
                'background_image': null,
                'height': null,
            },
            html: ['<h1>Header</h1><p>Para 1</p>','<h1>Header</h1><p>Para 2</p>','<h1>Header</h1><p>Para 3</p>','<h1>Header</h1><p>Para 4</p>','<h1>Header</h1><p>Para 5</p>'],
            shuffle: false,
            tickTime: 2000
        };
        $.extend(options,optionsInput);
        var output = {
            init: function(){
                this.buildOverallWrapper();
                this.resize();
                this.setFontSize();
                this.resetPosition();
                // this.ticker();
            },
            buildOverallWrapper: function(){
                wrapper.append('<div id="'+id+'"></div>');
                slideWrapper = $('#'+id);
                this.buildSlides();
            },
            buildSlides: function(){
                for(var i =0; i < rows; i++){
                    for(var z = 0; z < cols; z++){
                        // console.log('row',(i+1), "Col", (z+1));
                        slideWrapper.append('<div class="slideCell">Lorem ipsum dolor sit amet, consectetur adipiscing</div>');
                    }
                }
                slideWrapper.css('position','relative').find('.slideCell').each(function(i){
                    var cell=$(this);
                    percentage=(100/cols);
                    console.log('percentage',percentage);
                    cell.css({'position':'relative', 'display':'inline-block', 'background':options.css.background_image || options.css.background, 'padding': options.css.padding, 'height':options.css.height, 'margin-left': options.css.margin_left, 'margin-right': options.css.margin_right, 'margin-top':options.css.margin_top, 'margin-bottom':options.css.margin_bottom})
                    .css('width',(percentage-(Number(options.css.margin_left.replace('%',''))+Number(options.css.margin_right.replace('%',''))))+'%')
                    .html(options.html[i]);
                });
            },
            animate: function(pos){
                console.log('animate');
                wrapper.find('.slideCell').each(function(i){
                    console.log('each');
                    TweenMax.to($(this), 2, {top:pos[i][0], left:pos[i][1]});
                });
            },
            resetPosition: function(){
                console.log('resetPosition');
            },
            shuffle: function(){
                shuffleArr.arr = new Array();
                var temp_array = new Array();
                var new_array;
                if(shuffleArr.runEach == false){
                    $('#'+id).find('.slideCell').each(function(){
                        temp_array.push([$(this).position().top, $(this).position().left]);
                        console.log(temp_array);
                    });
                    shuffleArr.arr = temp_array;
                    shuffleArr.runEach = true;
                }
                else{

                }

                new_array = shuffleArray(shuffleArr.arr);
                this.animate(new_array);
                console.log(new_array);

                function shuffleArray(arr) {
                    for (var i = 0, shuffled = [], randomIndex = 0; i < arr.length; i++) {
                        randomIndex = Math.floor(Math.random() *  arr.length);
                    // If an item of this index already exists in the shuffled array, regenrate index.
                        while (shuffled.indexOf(arr[randomIndex]) !== -1) {
                            randomIndex = Math.floor(Math.random() *  arr.length);
                        }
                       shuffled.push(arr[randomIndex]);
                    }
                    return shuffled;
                }


            },
            ticker: function(){
                var $this=this;
                if(options.shuffle === true){
                    setInterval(function(){
                        $this.shuffle();
                    },options.tickTime);
                }
            },
            resize: function(){
                $(window).resize(function(){
                    console.log('resize');
                    slideWrapper.find('.slideCell').each(function(){
                        $(this).css('width',(percentage-(Number(options.css.margin_left.replace('%',''))+Number(options.css.margin_right.replace('%',''))))+'%');
                    });
                })
            },
            setFontSize: function(){
                console.log('setFontSize');
                $('#'+id).flowtype({
                   minimum : 500,
                   maximum : 1200
                });
            }
        }
        output.init();
    }

    $.fn.buildBtn = function(text,optionsInput, callback){
        var self=this;

        var options = {
            html: {
                start: '<div class="button">',
                end: '</div>'
            },
            css: {
                width: "auto",
                height: "auto",
                background: 'black',
                color: 'White',
                text_align: 'center',
                padding: '10px',
                display: 'inline-block',
                font_size: '1em',
                cursor: 'pointer'
            } 
        };
       
        $.extend(true,options,optionsInput);

        var output = {
            init: function(){
                this.buildWrapper();
                this.addEventListener();
            },
            buildWrapper: function(){
                self.append(options.html.start+text+options.html.end)
                .css({'width':options.css.width, 'height':options.css.height, 'background':options.css.background, 'color':options.css.color, 
                    'text-align': options.css.text_align, padding: options.css.padding, 'display': options.css.display, 'font-size': options.css.font_size, 'cursor': options.css.cursor});
            },
            addEventListener: function(){
                self.click(function(){
                    callback();
                });
            }
        }
        output.init();
    };

    $.fn.navigationBuild = function(arr,optionsInput){
        var self=this;
        var html = {
            start: '<div id="navigation">',
            middle: '',
            end: '</div>'
        }

        var options = {
            width: '100%',
            height: '100px',
            padding: '20px',
            text_align: 'center',
        }

        var output = {
            init: function(){
                this.buildWrapper();
            },
            linkUrls: function(){

            },
            buildWrapper: function(){
                for(var i = 0; i < arr.length; i++){
                    console.log(arr[i][0],arr[i][1]);
                    html.middle+='<a href="'+arr[i][1]+'">'+arr[i][0]+'</a>';

                }
                console.log(html.middle);
                self.append(html.start+html.middle+html.end);
                this.applyStyles();
            },
            applyStyles: function(){

                self.css({'height': options.height,})

                self.find('a').each(function(){
                    $(this).css({'padding':options.padding,'text-align':options.text_align, });
                });
            }
        }
        output.init();
    };

}( jQuery ));